FROM gradle:8.2.1-jdk17 AS buildstep
WORKDIR /app
COPY . .
RUN gradle bootJar

FROM eclipse-temurin:17-jre
COPY --from=buildstep /app/build/libs/*.jar /opt/wordcount/lib/app.jar
ENTRYPOINT ["java"]
CMD ["-jar", "/opt/wordcount/lib/app.jar"]
