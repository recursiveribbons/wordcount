package dev.robinsyl.wordcount.rest;

import dev.robinsyl.wordcount.services.WordFrequencyAnalyzer;
import dev.robinsyl.wordcount.types.WordFrequency;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;

import java.util.Map;
import java.util.stream.Collectors;

@Path("/wordcount")
public class WordCountEndpoint {

    final WordFrequencyAnalyzer service;

    public WordCountEndpoint(WordFrequencyAnalyzer service) {
        this.service = service;
    }

    @POST
    @Path("/frequency/highest")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
    public Integer highestFrequency(@FormParam("text") String text) {
        return service.calculateHighestFrequency(text);
    }

    @POST
    @Path("/frequency/word")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
    public Integer frequencyByWord(@FormParam("text") String text, @QueryParam("word") String word) {
        return service.calculateFrequencyForWord(text, word);
    }

    @POST
    @Path("/frequency/top")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Integer> topWords(@FormParam("text") String text, @QueryParam("limit") Integer limit) {
        return service.calculateMostFrequentNWords(text, limit)
                .stream()
                .collect(Collectors.toMap(WordFrequency::getWord, WordFrequency::getFrequency));
    }
}
