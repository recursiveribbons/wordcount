package dev.robinsyl.wordcount.services;

import dev.robinsyl.wordcount.types.WordFrequency;
import dev.robinsyl.wordcount.types.WordFrequencyRecord;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;

@Service
public class WordFrequencyAnalyzerImpl implements WordFrequencyAnalyzer {
    @Override
    public int calculateHighestFrequency(String text) {
        if (text == null || text.isBlank()) {
            // invalid request
            return 0;
        }

        Map<String, Long> frequencyMap = toFrequencyMap(text);
        return frequencyMap.values()
                .stream()
                .max(Comparator.naturalOrder())
                .map(Long::intValue)
                .orElse(0);
    }

    @Override
    public int calculateFrequencyForWord(String text, String word) {
        if (text == null || text.isBlank() || word == null || word.isBlank()) {
            // invalid request
            return 0;
        }

        Map<String, Long> frequencyMap = toFrequencyMap(text);
        // lower case, as case-insensitive matching is expected
        return frequencyMap.getOrDefault(word.toLowerCase(), 0L).intValue();
    }

    @Override
    public List<WordFrequency> calculateMostFrequentNWords(String text, int n) {
        if (text == null || text.isBlank() || n == 0) {
            // invalid request
            return Collections.emptyList();
        }

        Map<String, Long> frequencyMap = toFrequencyMap(text);
        return frequencyMap.entrySet()
                .stream()
                .sorted(Map.Entry.<String, Long>comparingByValue(Comparator.reverseOrder()).thenComparing(Map.Entry.comparingByKey()))
                .limit(n)
                .map(entry -> (WordFrequency) new WordFrequencyRecord(entry.getKey(), entry.getValue().intValue()))
                .toList();
    }

    private Map<String, Long> toFrequencyMap(String text) {
        // lower case, as case-insensitive matching is expected
        String lowerCase = text.toLowerCase();
        // split string by any separator, defined as characters that aren't a-z or A-Z
        return Arrays.stream(lowerCase.split("[^a-zA-Z]+"))
                .parallel().unordered()
                .filter(not(String::isBlank))
                .collect(Collectors.groupingByConcurrent(Function.identity(), Collectors.counting()));
    }
}
