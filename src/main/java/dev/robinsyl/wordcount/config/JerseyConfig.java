package dev.robinsyl.wordcount.config;

import dev.robinsyl.wordcount.rest.WordCountEndpoint;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(WordCountEndpoint.class);
    }

}
