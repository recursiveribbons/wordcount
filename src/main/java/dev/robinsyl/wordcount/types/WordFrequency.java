package dev.robinsyl.wordcount.types;

public interface WordFrequency {
    String getWord();

    int getFrequency();
}
