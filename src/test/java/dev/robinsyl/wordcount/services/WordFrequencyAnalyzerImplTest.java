package dev.robinsyl.wordcount.services;

import dev.robinsyl.wordcount.types.WordFrequency;
import dev.robinsyl.wordcount.types.WordFrequencyRecord;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

class WordFrequencyAnalyzerImplTest {

    WordFrequencyAnalyzer service = new WordFrequencyAnalyzerImpl();

    @Nested
    class CalculateHighestFrequency {

        @ParameterizedTest
        @ValueSource(strings = {"Lorem Ipsum Lorem Lorem Lorem", "Lorem ipsum lorem lOrEm Ipsum LOREM", "Lorem&^Ipsum  -Lorem Lorem \u00A0\n\tLorem", "A A A A B B B B"})
        void expectFour(String testString) {
            int result = service.calculateHighestFrequency(testString);
            assertThat(result, equalTo(4));
        }

        @ParameterizedTest
        @NullAndEmptySource
        @ValueSource(strings = "\u00A0\n\t")
        void expectZero(String testString) {
            int result = service.calculateHighestFrequency(testString);
            assertThat(result, equalTo(0));
        }

    }

    @Nested
    class CalculateFrequencyForWord {

        @ParameterizedTest
        @ValueSource(strings = {"Lorem Ipsum Lorem Lorem Lorem", "Lorem ipsum lorem lOrEm Ipsum LOREM", "Lorem&^Ipsum  -Lorem Lorem \u00A0\n\tLorem"})
        void expectFour(String testString) {
            int result = service.calculateFrequencyForWord(testString, "Lorem");
            assertThat(result, equalTo(4));
        }

        @ParameterizedTest
        @NullAndEmptySource
        @ValueSource(strings = {"\u00A0\n\t", "The Bear"})
        void expectZero(String testString) {
            int result = service.calculateFrequencyForWord(testString, "Lorem");
            assertThat(result, equalTo(0));
        }

    }

    @Nested
    class CalculateMostFrequentNWords {

        @ParameterizedTest
        @ValueSource(strings = {"A C A A B C B", "A C a A B c B", "A&^C  -A \u00A0\n\tA B C B"})
        void standardTest() {
            String testString = "A C A A B C B";
            List<WordFrequency> wordFrequencies = service.calculateMostFrequentNWords(testString, 3);

            assertThat(wordFrequencies, contains(new WordFrequencyRecord("a", 3), new WordFrequencyRecord("b", 2), new WordFrequencyRecord("c", 2)));
        }

        @ParameterizedTest
        @NullAndEmptySource
        @ValueSource(strings = "\u00A0\n\t")
        void expectZero(String testString) {
            List<WordFrequency> wordFrequencies = service.calculateMostFrequentNWords(testString, 5);
            assertThat(wordFrequencies, empty());
        }

        @Test
        void nIsZero() {
            List<WordFrequency> wordFrequencies = service.calculateMostFrequentNWords("something", 0);
            assertThat(wordFrequencies, empty());
        }

    }
}