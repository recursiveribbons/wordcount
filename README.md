# Word Count

This is an implementation of a word counter based on requirements from Ordina.

## How to run

### IDE

Import the project into your IDE of choice and run
the [WordCountApplication](src/main/java/dev/robinsyl/wordcount/WordCountApplication.java) class

### Gradle

Run `./gradlew bootRun` in the terminal

### Docker

Run the following in the terminal

```bash
docker build -t wordcount .
docker run --rm -it -p 8080:8080 wordcount
```

### Docker Compose

Run `docker-compose up --build` in the terminal

## Using the API

Make a POST request with the media type `application/x-www-form-urlencoded`
and the field `text` containing the text to be analyzed.

### Highest Frequency

The path is `/wordcount/frequency/highest`.
The API returns an integer with media type `text/plain`.

### Frequency by word

The path is `/wordcount/frequency/word`.
Provide a query parameter `word` with the word to be searched.
The API returns an integer with media type `text/plain`.

### Most frequent n words

The path is `/wordcount/frequency/top`.
The API returns a map with media type `application/json`. The key is the word and the value is the frequency.

## Architecture

The application is based on Spring Boot 3 and Jersey (Jakarta EE), running on Java 17.
GitLab CI is configured for tests and building a Docker image.

## Future considerations

I was unable to get Swagger/Open API working with Spring Boot + Jersey, perhaps a method may be found in the future.
